var should = require('should');
var compiler = require('../src/bootstrap');

describe('bootstrap-compiler', function() {
  it('plain code', function() {
    compiler('(JSCode "console.log(\\"hello world\\")")')
      .should.eql('console.log("hello world");');
    compiler('(JSCode "console.log("' +
             '  (JSCode "\\"hello world\\"") ' +
             '  ")")')
      .should.eql('console.log("hello world");');
  });
  it('function call', function() {
    compiler('(alert "hello world")')
      .should.eql('alert("hello world");');
  });
  it('macro', function() {
    compiler('(defmacro mymacro (JSCode "(function(arg){return [arg];})")) (mymacro "abc")')
      .should.eql('"abc";');
    compiler('(defmacro mymacro (JSCode "(function(arg){return [\\"def\\"];})")) ' +
             '(JSise ' +
               '(defmacro mymacro (JSCode "(function(arg){return [\\"ghi\\"];})"))' +
               '(mymacro "abc"))')
      .should.eql('"ghi";');
    compiler('(JSise ' +
               '(defmacro mymacro (JSCode "(function(arg){return [\\"ghi\\"];})"))' +
               '(mymacro "abc"))')
      .should.eql('"ghi";');
  });
});
