var should = require('should');
var compiler = require('../src/bootstrap');
var parser = compiler.parser;

describe('bootstrap-parser', function() {
  it('parse paren', function() {
    parser('()').should.eql([[]]);
    parser('(())').should.eql([[[]]]);
    parser('(()())').should.eql([[[],[]]]);
  });
  it('parse spc', function() {
    parser('  ').should.eql([]);
    parser('  (  )  ').should.eql([[]]);
    parser('  (  (  )  )  ').should.eql([[[]]]);
  });
  it('parse id', function() {
    parser(' id ').should.eql([new String('id')]);
    parser('(id)').should.eql([[new String('id')]]);
    parser('(id1 id2)').should.eql(
      [[new String('id1'),new String('id2')]]);
    parser('(console.log error)').should.eql(
      [[new String('console.log'),new String('error')]]);
    parser('(.alert window  error)').should.eql(
      [[new String('.alert'),new String('window'),new String('error')]]);
  });
  it('parse str', function() {
    parser('"abc"').should.eql(['abc']);
    parser('("abc")').should.eql([['abc']]);
    parser('("abc" ("def"))').should.eql([['abc',['def']]]);
    parser('"a\\"bc"').should.eql(['a\"bc']);
    parser('"a\\bc"').should.eql(['abc']);
    parser('"a\\nbc"').should.eql(['a\nbc']);
  });
});
