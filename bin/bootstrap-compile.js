#!/usr/bin/env node
var compiler = require('../src/bootstrap');
var code = '';
process.stdin.setEncoding('utf8');
process.stdin.resume();
process.stdin.on('data', function(data) {
  code += data;
});
process.stdin.on('end', function() {
  process.stdout.write(compiler(code));
});
