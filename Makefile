STDLIB_SRC= src/stdlib.jsise
BIN_SRC= src/bin/jsise.jsise $(STDLIB_SRC)
LIB_SRC=  src/compiler.jsise $(STDLIB_SRC)
COMPILER= bin/jsise lib/compiler.js src/stdlib.jsise
JSISE= ./bin/jsise compile

all: compiler registers jsise.js

compiler: bin/jsise lib/compiler.js

lib/compiler.js: $(LIB_SRC) bin/jsise1 lib/compiler1.js
	mkdir -p lib
	./bin/jsise1 compile -b stage1 -i $< -o $@

bin/jsise: $(BIN_SRC) bin/jsise1 lib/compiler1.js
	./bin/jsise1 compile -b stage1 -i $< -o $@
	chmod 755 $@

lib/compiler1.js: $(LIB_SRC) bin/jsise1 src/bootstrap.js
	mkdir -p lib
	./bin/jsise1 compile -b bootstrap -i $< -o $@

bin/jsise1: $(BIN_SRC) bin/bootstrap-compile.js src/bootstrap.js
	cat $(STDLIB_SRC) $< | ./bin/bootstrap-compile.js > $@
	chmod 755 $@

registers: register-bootstrap register

register: lib/register.js

register-bootstrap: lib/register-bootstrap.js

lib/register.js: src/register.jsise src/register-lib.jsise $(COMPILER)
	mkdir -p lib
	$(JSISE) -i $< -o $@

lib/register-bootstrap.js: src/register-bootstrap.jsise src/register-lib.jsise $(COMPILER)
	mkdir -p lib
	$(JSISE) -i $< -o $@

check: check-driver check-compiler

check-driver: bin/jsise bin/jsise2 bin/jsise3
	diff bin/jsise bin/jsise2 || echo Error!
	diff bin/jsise2 bin/jsise3 || echo Error!

check-compiler: lib/compiler.js lib/compiler2.js lib/compiler3.js
	diff lib/compiler.js lib/compiler2.js|| echo Error!
	diff lib/compiler2.js lib/compiler3.js|| echo Error!

lib/compiler2.js: $(LIB_SRC) bin/jsise lib/compiler.js
	mkdir -p lib
	./bin/jsise compile -i $< -o $@

bin/jsise2: $(BIN_SRC) bin/jsise lib/compiler.js
	./bin/jsise compile -i $< -o $@
	chmod 755 $@

lib/compiler3.js: $(LIB_SRC) bin/jsise2 lib/compiler2.js
	mkdir -p lib
	./bin/jsise2 compile -b stage2  -i $< -o $@

bin/jsise3: $(BIN_SRC) bin/jsise2 lib/compiler2.js
	./bin/jsise2 compile -b stage2 -i $< -o $@
	chmod 755 $@

jsise.js: $(LIB_SRC) bin/jsise src/gen-jsise-js.jsise src/libweb.jsise
	bin/jsise exec -i src/gen-jsise-js.jsise > jsise.js

clean:
	rm -rf lib bin/jsise bin/jsise1 bin/jsise2 bin/jsise3 jsise.js

test: check registers lib/compiler1.js lib/compiler.js
	node_modules/mocha/bin/mocha -R spec --compilers jsise:lib/register

