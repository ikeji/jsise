(var JSCode (new String "JSCode"))
(var JSise (new String "JSise"))

(defn equalString (a b)
  (and
    (instanceof a String)
    (instanceof b String)
    (== (.valueOf a) (.valueOf b))))

(def-function readOnlyClone (obj)
  (def-function Clone ())
  (set! Clone.prototype obj)
  (return (new Clone)))

(defn L (module msg)
  (if (!= (typeof require) "undefined")
    ((.debuglog (require "util") module) msg)
    undefined))

(defn loadStdlib ()
  (cond
    require (.readFileSync
              (require "fs")
              (.join (require "path")
                     __dirname ".." "src" "stdlib.jsise"))
    (throw-expression (new Error "Unsupported environment"))))

(var stdlib (|| stdlib null))
(defn getStdlib ()
  (if stdlib
    stdlib
    (set! stdlib (loadStdlib))))

(def-function jsise (src opt_opt)
  (L "jsise.compiler" "--jsise--")
  (L "jsise.compiler" src)
  (L "jsise.compiler" opt)
  (var opt (|| opt_opt (object)))
  (var code (if opt.withStdlib
              (+ (getStdlib) src)
              src))
  (return (jsise.codegen
            (jsise.convert
              (jsise.macro
                (jsise.parser code)
                opt)))))

(defmethod jsise.parser (str)
  (var i 0)
  (var state null)
  (var parents (list (list)))
  (var token "")

  (var states (object))

  (defmacro defstate
    (syntax-macro
      (aset! states (symbol-name ~0) (function-with-name ~0 ()
        (var c (aget str i))
        ~1-))))

  (defstate paren
    (cond
      (== c "(")
        ((function ()
          (var paren (list))
          (.push (aget parents 0) paren)
          (.unshift parents paren)))
      (or (== c " ") (== c "\n") (== c "\t"))
        undefined
      (== c ")")
        ((function ()
          (when (== parents.length 1)
            (throw (new Error "Too many closed paren")))
          (.shift parents)))
      (== c "\"")
        (set! state states.stringLiteral)
      ((function ()
        (set! state states.id)
        (state)))))

  (defstate id
    (ifs (or
           (== c "(")
           (== c ")")
           (== c "\"")
           (== c " ")
           (== c "\n")
           (== c "\t")) (
      (.push (aget parents 0) (new String token))
      (set! token "")
      (set! state states.paren)
      (state)
    ) (
      (set! token (+ token c))
    )))

  (defstate stringLiteral
    (cond
      (== c "\\")
        (set! state states.esc)
      (== c "\"")
        ((function ()
          (.push (aget parents 0) token)
          (set! token "")
          (set! state states.paren)))
      (set! token (+ token c))))

  (defstate esc
    (cond
      (== c "n")
        (set! token (+ token "\n"))
      (== c "t")
        (set! token (+ token "\t"))
      (set! token (+ token c)))
    (set! state states.stringLiteral))

  (set! state states.paren)
  (for undefined (< i str.length) (++ i)
    (state))
  (when (!= parents.length 1)
    (throw (new Error "unclosed paren")))
  (when (== state states.stringLiteral)
    (throw (new Error "unclosed string")))
  (return (first parents))
)

(defmethod jsise.macro (ast opt opt_macros)
  (when (not (instanceof ast Array))
    (return ast))
  (var macros (|| opt_macros (object)))
  (defmethod macros.defmacro (name opt_bodys)
    (// "Apis for macro")
    (var __compiler "jsise")
    (var __parser jsise.parser)
    (var __macro jsise.macro)
    (var __filename opt.filename)
    (var __macros macros)
    (aset! macros (.valueOf name)
           (eval
             (jsise.codegen
               (jsise.convert
                 (jsise.macro 
                   (rest (argument-array))
                   opt
                   macros)))))
    (return (list)))
  (return (reduce-left (fn-with-name macroExpand (ret exp)
                         (if (or (not (instanceof exp Array))
                                 (not (instanceof (aget exp 0) String))
                                 (not (aget macros (.valueOf (aget exp 0)))))
                           (concat ret (list (jsise.macro exp opt (readOnlyClone macros))))
                           (reduce-left macroExpand
                                        ret
                                        (.apply (aget macros (.valueOf (aget exp 0)))
                                                undefined
                                                (rest exp)))))
                       (list)
                       ast)))

(defmethod jsise.convert (ast)
  (def-function convertExpression (exp)
    (try (
      (L "jsise.convert" exp)
      (when (is-string? exp)
        (var esc (list JSCode "\""))
        (for (var i 0) (< i exp.length) (++ i)
          (var c (.charAt exp i))
          (cond
            (or (== c "\"")
                (== c "\\"))
              (.push esc (+ "\\" c))
            (== c "\n")
              (.push esc "\\n")
            (== c "\t")
              (.push esc "\\t")
            (.push esc c)))
        (.push esc "\"")
        (return esc))
      (when (instanceof exp String)
        (return (list JSCode (.valueOf exp))))
      (when (not (instanceof exp Array))
        (L "jsise.convert" exp)
        (throw (new Error (+ "Invalid ast:" exp))))
      (when (equalString JSise (aget exp 0))
        (return (concat (list JSCode)
                        (map convertExpression
                             (rest exp)))))
      (when (equalString JSCode (aget exp 0))
        (return (map (fn (e i)
                       (if (or (== i 0)
                               (is-string? e))
                         e
                         (convertExpression e)))
                     exp)))
      (var method null)
      (var args null)
      (ifs (and (instanceof (aget exp 0) String)
                (== (aget (.valueOf (aget exp 0)) 0) ".")) (
        (set! method
              (list JSCode (convertExpression (aget exp 1)) (.valueOf (aget exp 0))))
        (set! args (rest (rest exp)))
      ) (
        (set! method (list JSCode (convertExpression (aget exp 0))))
        (set! args (rest exp))
      ))
      (return (concat method
                      (list "(")
                      (reduce-left (fn (arr v i)
                                     (if (!= i 0)
                                       (concat arr (list "," v))
                                       (concat arr (list v))))
                                   (list)
                                   (map convertExpression args))
                      (list ")")))
    ) catch e (
      (set! e.message (+ (+ e.message "\n at ") (JSON.stringify exp)))
      (throw e)
    )))
  (return (map (fn (statement) (list JSCode (convertExpression statement) ";"))
               ast)))

(defmethod jsise.codegen (ast)
  (return
    (.join
      (map
        (function-with-name convertToJSCode (statement)
          (when (or (not (instanceof statement Array))
                    (not (equalString JSCode (aget statement 0))))
            (throw (new Error (+ "Invalid code: " (JSON.stringify statement)))))
          (return
            (.join (.map (rest statement)
                         (fn (codeOrStatement)
                           (if (is-string? codeOrStatement)
                             codeOrStatement
                             (convertToJSCode codeOrStatement))))
                   "")))
        ast)
      "\n\n")))

(when (&& (!= (typeof module) "undefined") module.exports)
  (set! module.exports jsise))
