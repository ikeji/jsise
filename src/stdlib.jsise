(defmacro // (JSCode "(function(){ return [new String('undefined')]; })"))
(defmacro debugger (JSCode "(function(){ return [new String('debugger')]; })"))

(defmacro quote
  (JSCode "(function quote(exp) {
    var JSCode = new String('JSCode');
    JSCode.equals = function (other) {
      return other instanceof String && other.valueOf() == this.valueOf();
    };
    function toStr(str) {
      var esc = [JSCode, '\"'];
      for(var i=0;i<exp.length;i++) {
        var c = exp.charAt(i);
        if (c=='\"' || c == '\\\\') {
          esc.push('\\\\' + c);
        } else if (c == '\\n') {
          esc.push('\\\\n');
        } else {
          esc.push(c);
        }
      }
      esc.push('\"');
      return esc;
    }
    if (typeof exp == 'string') {
      return [toStr(exp)];
    }
    if (exp instanceof String) {
      return [[JSCode, 'new String(', toStr(exp.valueOf()), ')']];
    }
    if (!(exp instanceof Array)) {
      throw new Error('invalid ast');
    }
    return [[JSCode, '['].concat(
        exp.map(quote).reduce(function(arr, v, i) {
          if (i != 0) {
            return arr.concat([',', v[0]]);
          } else {
            return arr.concat([v[0]]);
          }
        }, []), [']'])];
  })"))

(defmacro syntax-macro
  (JSCode "(function(var_args) {
    var arg = Array.prototype.slice.call(arguments, 0);
    return [[new String('JSCode'), '(function() { ',
      'var template = ' ].concat([
          [new String('JSise'),
            [new String('quote'), arg]],
      ';',
      'var targ = arguments;',
      'function expand(template) {',
      '  if (template instanceof String) {',
      '    var r = (/^[~](([~]+)?([0-9]+)(-)?)$/).exec(template.valueOf());',
      '    if (r) {',
      '      if (r[2]) {',
      '        return [new String(r[1])];',
      '      } else if (r[4]) {',
      '        return Array.prototype.slice.call(targ, parseInt(r[3]));',
      '      } else {',
      '        return [targ[parseInt(r[3])]];',
      '      }',
      '    }',
      '  }',
      '  if (template instanceof Array) {',
      '    return [template.reduce(function(o,t) {', 
      '      return o.concat(expand(t));',
      '    }, [])];',
      '  }',
      '  return [template];',
      '}',
      'return expand(template)[0];',
      '})'])];
  })"))

(defmacro shallow-quote-list
  (JSCode "(function(varg){
    return [[new String('JSCode')].concat(varg.reduce(function(a,v,i) {
      if (i!=0) {
        return a.concat([',', [ new String('JSise'), v]]);
      } else {
        return a.concat([[new String('JSise'), v]]);
      }
    },[]))];
  })"))

(defmacro statement-list
  (JSCode "(function (statements){
    return [statements.reduce(function(ar, i) {
      return ar.concat([i, [new String('JSCode'), ';\\n']]);
    }, [new String('JSise')])];
  })"))

(defmacro fn
  (syntax-macro
    (JSCode "(function("
              (JSise (shallow-quote-list ~0))
            ") {"
            "  return "
              (JSise ~1)
            "})")))

(defmacro fn-with-name
  (syntax-macro
    (JSCode "(function " ~0 " ("
              (JSise (shallow-quote-list ~1))
            ") {"
            "  return "
              (JSise ~2)
            "})")))

(defmacro function
  (syntax-macro
    (JSCode "(function("
              (JSise (shallow-quote-list ~0))
            ") { "
              (statement-list (~1-))
            "})")))

(defmacro function-with-name
  (syntax-macro
    (JSCode "(function " ~0 " ("
              (JSise (shallow-quote-list ~1))
            ") { "
              (statement-list (~2-))
            "})")))

(defmacro return
  (syntax-macro (JSCode "return " (JSise ~0))))

(defmacro list
  (syntax-macro (JSCode "[" (shallow-quote-list (~0-)) "]")))

(defmacro shallow-quote
  (fn (body)
    (list (.concat (quote (list)) body))))

(defmacro concat
  (syntax-macro (Array.prototype.concat.apply (list) (shallow-quote (~0-)))))

(defmacro cons
  (syntax-macro (concat (list ~0 ) ~1)))

(defmacro if
  (syntax-macro (JSCode "(" (JSise ~0) "?" (JSise ~1) ":" (JSise ~2) ")")))

(defmacro ifs
  (syntax-macro
    (JSCode "if (" (JSise ~0) ") {"
      (statement-list ~1)
    "} else {"
      (statement-list ~2)
    "}")))

(defmacro get
  (syntax-macro (JSCode "(" (JSise ~0) "." (JSise ~1) ")")))

(defmacro aget
  (syntax-macro (JSCode "(" (JSise ~0) "[" (JSise ~1) "])")))

(defmacro aset!
  (syntax-macro (JSCode "(" (JSise ~0) "[" (JSise ~1) "] = " (JSise ~2) ")")))

(defmacro var
  (syntax-macro (JSCode "var " (JSise ~0) " = " (JSise ~1))))

(defmacro def-binary-operator
  (syntax-macro
    (defmacro ~0
      (syntax-macro (JSCode "(" (JSise ~~0) " " ~1 " " (JSise ~~1 ) ")")))))

(def-binary-operator * *)
(def-binary-operator / /)
(def-binary-operator plus-operator +)
(def-binary-operator - -)
(def-binary-operator % %)
(def-binary-operator = =)
(def-binary-operator == ==)
(def-binary-operator != !=)
(def-binary-operator === ===)
(def-binary-operator !== !==)
(def-binary-operator && &&)
(def-binary-operator || ||)
(def-binary-operator < <)
(def-binary-operator > >)
(def-binary-operator <= <=)
(def-binary-operator => =>)
(def-binary-operator instanceof instanceof)

(def-binary-operator ^ ^)
(def-binary-operator | |)
(def-binary-operator & &)
(def-binary-operator << <<)
(def-binary-operator >> >>)

(defmacro not
  (syntax-macro (JSCode "(!" (JSise ~0) ")")))

(defmacro set!
  (syntax-macro (= ~0-)))

(defmacro symbol->string
  (syntax-macro (.valueOf ~0)))

(defmacro symbol-name
  (fn (symbol)
    (list (symbol->string symbol))))

(defmacro symbol-name-list
  (fn (symbolList)
    (list
      (cons (quote list)
            (.map symbolList
                  (fn (n) (symbol->string n)))))))

(defmacro let1
  (syntax-macro
    ((fn (~0)
       ~2
     ) ~1)))

(defmacro zip
  (syntax-macro
    (let1 ar ~0
      (.map (aget ar 0) (fn (_ i)
        (.map ar (fn (a) (aget a i))))))))

(defmacro odd?
  (syntax-macro
    (== (% ~0 2) 0)))

(defmacro even?
  (syntax-macro (not (odd? ~0))))

(defmacro odd-list
  (syntax-macro
    (.filter ~0 (fn (_ i) (odd? i)))))

(defmacro even-list
  (syntax-macro
    (.filter ~0 (fn (_ i) (even? i)))))

(defmacro arraylike->array
  (syntax-macro (Array.prototype.slice.call ~0 0)))

(defmacro first
  (syntax-macro (aget ~0 0)))

(defmacro second
  (syntax-macro (aget ~0 1)))

(defmacro 1st
  (syntax-macro (aget ~0 0)))
(defmacro 2nd
  (syntax-macro (aget ~0 1)))
(defmacro 3rd
  (syntax-macro (aget ~0 2)))
(defmacro 4th
  (syntax-macro (aget ~0 3)))
(defmacro 5th
  (syntax-macro (aget ~0 4)))
(defmacro 6th
  (syntax-macro (aget ~0 5)))
(defmacro 7th
  (syntax-macro (aget ~0 6)))
(defmacro 8th
  (syntax-macro (aget ~0 7)))
(defmacro 9th
  (syntax-macro (aget ~0 8)))
(defmacro 10th
  (syntax-macro (aget ~0 9)))

(defmacro rest
  (syntax-macro (.slice ~0 1)))

(defmacro object
  (fn (var_args)
    (list
      (concat
        (quote (JSCode "{"))
        (.reduce
          (.map (zip (list (odd-list (arraylike->array arguments))
                           (even-list (arraylike->array arguments))))
                (fn (a)
                  (list (list (quote JSise) (1st a)) ":"
                  (list (quote JSise) (2nd a)))))
          (fn (r a i)
            (if (== i 0)
              (concat r a)
              (concat r (list ",") a)))
          (list))
        (list "}")))))

(defmacro try
  (syntax-macro
    (JSCode "try {"
              (JSise (statement-list ~0))
            "} catch (" ~2 ") {"
              (JSise (statement-list ~3))
            "}")))

(defmacro ignore-error
  (syntax-macro
    (JSCode "try {"
              (JSise (statement-list (~0-)))
            "} catch {}")))

(defmacro #!
  (syntax-macro (JSCode "#!" ~0 "\n")))

(defmacro new
  (syntax-macro (JSCode "(new " ~0 "(" (shallow-quote-list (~1-)) "))")))

(defmacro when
  (syntax-macro
    (JSCode "if (" (JSise ~0) ") {\n"
      (statement-list (~1-))
    "}")))

(defmacro throw
  (syntax-macro (JSCode "throw " (JSise ~0))))

(defmacro for-each-key
  (syntax-macro (JSCode "for (var " ~0 " in " ~1 "){"
    (JSise (statement-list (~2-)))
  "}")))





(defmacro for-each
  (syntax-macro (.forEach ~2 (function (~0) ~3-))))

(defmacro repeat-times
  (syntax-macro (for (var ~0 0) (< ~0 ~1) (set! ~0 (+ ~0 1)) ~2-)))

(defmacro reduce-left
  (// "args: callback inital arr")
  (syntax-macro (.reduce ~2 ~0 ~1)))

(defmacro map
  (syntax-macro (.map ~1 ~0)))

(defmacro filter-only
  (syntax-macro (.filter ~1 ~0)))

(defmacro flatten
  (syntax-macro (Array.prototype.concat.apply [] ~0)))

(defmacro argument-array
  (syntax-macro (arraylike->array arguments)))

(defmacro ->
  (fn (var_args)
    (list
      (reduce-left
        (fn (form operation)
          (cons (1st operation)
                (cons form (rest operation))))
        (1st (argument-array))
        (rest (argument-array))))))

(defmacro defn
  (syntax-macro (var ~0 (fn-with-name ~0 ~1 ~2))))

(defmacro def-function
  (syntax-macro (var ~0 (function-with-name ~0 ~1 ~2-))))

(defmacro cond
  (fn (var_args)
    (list
      (if (<= arguments.length 1)
        (|| (aget arguments 0) (quote undefined))
        (list (quote if) (aget arguments 0) (aget arguments 1)
          (if (== arguments.length 2)
            (quote undefined)
            (concat (quote (cond))
                    (rest (rest (argument-array))))))))))

(defmacro def-varlength-binary-operator
  (syntax-macro
    (defmacro ~0
      (fn (var_args)
        (cond
          (== arguments.length 0) (quote (true))
          (== arguments.length 1) (argument-array)
          (list
            (list (quote ~1)
                  (aget arguments 0)
                  (concat (quote (~0))
                          (rest (argument-array))))))))))

(def-varlength-binary-operator and &&)
(def-varlength-binary-operator or ||)
(def-varlength-binary-operator + plus-operator)

(defmacro defmethod
  (syntax-macro (set! ~0 (function ~1 ~2-))))

(defmacro is-string?
  (syntax-macro (== (typeof ~0) "string")))

(defmacro is-symbol?
  (syntax-macro (instanceof ~0 String)))

(defmacro is-array?
  (syntax-macro (instanceof ~0 Array)))

(defmacro reg-exp
  (syntax-macro (new RegExp ~0-)))

(defmacro re-match
  (syntax-macro (.match ~1 (reg-exp ~0))))

(defmacro debug-log
  (syntax-macro ((.debuglog (require "util") ~0) ~1-)))

(defmacro is-symbol-of?
  (syntax-macro ((fn (n) (and (is-symbol? n)
                              (== (.valueOf n) ~1))) ~0)))

(defmacro quasiquote
  (fn (template)
    (rest
      ((fn-with-name quasiquoteInternal (node)
         (cond
           (or (is-string? node) (is-symbol? node))
             (list (quote list) (list (quote quote) node))
           (is-symbol-of? (1st node) "unquote")
             (list (quote list) (2nd node))
           (is-symbol-of? (1st node) "unquote-splicing")
             (2nd node)
           (list (quote list)
                 (cons (quote concat)
                       (map (fn (i) (quasiquoteInternal i))
                            node)))))
       template))))


(defmacro require-with-option
  (function (var_args)
    (var args (argument-array))
    (defn getJsNameFromFileNameSymbol (fname)
      (new String
           (->
             fname
             (.valueOf)
             (.replace (reg-exp ".*/") "")
             (.replace (reg-exp "-[a-z]" "g")
                       (fn (c) (.toUpperCase (aget c 1)))))))

    (when (and (== args.length 3)
               (is-symbol? (2nd args))
               (== (symbol->string (2nd args)) "as"))
      (return (quasiquote ((var (unquote (aget args 2))
                                (require (unquote (symbol->string (1st args)))))))))
    (return (quasiquote ((var (unquote (getJsNameFromFileNameSymbol (1st args)))
                              (require (unquote (symbol->string (1st args))))))))))

(defmacro requires
  (fn (var_args)
    (map (fn (m)
           (list (quote JSise)
                 (cons (quote require-with-option) m)
                 (list (quote JSCode)
                       ";")))
         (argument-array))))

(defmacro import
  (function (filename)
    (// "
    // Search path is:
    // 1. Relative path if it's start with './' or '..'
    // 2. Package path.
    // 3. node_module path.
    ")
    (when (== __compiler "bootstrap")
      (throw (new Error "Import doesn't support bootstrap compiler")))
    (when (not require)
      (throw (new Error "Import is not supported")))
    (when (not __filename)
      (throw (new Error "Unknown source file")))
    (debug-log "stdlib.import" "filename" filename)
    (debug-log "stdlib.import" "__filename" __filename)
    (requires (fs) (path))
    (def-function findBasePath (basefile filename)
      (defn findProject (dir)
        (if (fs.existsSync (path.join dir "package.json"))
          dir
          (if (== dir "/")
            undefined
            (findProject (path.dirname dir)))))
      (var projectdir (findProject (path.dirname basefile)))
      (debug-log "stdlib.import" "projectdir" projectdir)
      (var projectpath (path.join projectdir filename))
      (when (fs.existsSync projectpath)
        (return projectpath))
      (var nodeModulesPath (path.join projectdir "node_modules" filename))
      (when (fs.existsSync nodeModulesPath)
        (return nodeModulesPath))
      (return undefined))
    (var fname
         (if (or (== (.substr filename 0 2) "./")
                 (== (.substr filename 0 3) "../"))
           (path.resolve (path.dirname __filename)
                         filename)
           (findBasePath __filename filename)))
    (debug-log "stdlib.import" "fname" fname)
    (when (not fname)
      (throw (new Error (+ "imported file not found : " filename))))
    (return (__macro
      (__parser
        (+ (fs.readFileSync fname) ""))
      (object filename fname)
      __macros))))

(defmacro make-map
  (syntax-macro
    (reduce-left (function (m kv)
                   (aset! m (1st kv) (2nd kv))
                   (return m))
                 (object)
                 ~0)))

(defmacro throw-expression
  (syntax-macro ((function () (throw ~0)))))

(defmacro -->
  (fn (var_args)
    (list (concat
      (quote (-> (Promise.resolve)))
      (map (fn (exp) (quasiquote (.then (unquote exp))))
           (argument-array))))))

(defmacro async-function
  (syntax-macro (fn ~0 (--> ~1-))))

(defmacro while
  (syntax-macro
    (JSCode "while (" (JSise ~0) ") {"
      (JSise (statement-list (~1-)))
    "}")))

(defmacro define-macro
  (syntax-macro
    (defmacro ~0 ~1)))

(define-macro //
  (JSCode "(function() { return []; })"))

(// "New macros
- SHOULD NOT depends macro defined above
- SHOULD follow naming convention.
- SHOULD have comments.
- SHOULD NOT use dot syntax.
")

(define-macro syntax-template
  (syntax-macro (syntax-macro ~0-)))

(define-macro define-macro-with-comment
  (syntax-template
    (define-macro ~0 ~2)))

(define-macro syntax-template-with-comment
  (syntax-template (syntax-template ~1-)))

(define-macro-with-comment define-function
  "define javascript function.
  ~0  symbol name.
  ~1  list of parameters.
  ~2- statements.
  This returns statement."
  (syntax-template
    (var ~0 (function-with-name ~0 ~1 ~2-))))

(define-macro-with-comment define-function-with-comment
  "define javascript function.
  ~0  symbol name.
  ~1  list of parameters.
  ~2  Comment.
  ~3- statements.
  This returns statement."
  (syntax-template
    (var ~0 (function-with-name ~0 ~1 ~3-))))

(define-macro-with-comment array-get
  "Get an item from array or object.
  ~0 array.
  ~1 index."
  (syntax-template
    (JSCode "(" (JSise ~0) "[" (JSise ~1) "])")))

(define-macro-with-comment array-set!
  "Set an value to array or object.
  ~0 array.
  ~1 index.
  ~2 value."
  (syntax-template
    (JSCode "(" (JSise ~0) "[" (JSise ~1) "] = " (JSise ~2) ")")))

(define-macro-with-comment .
  "This macro has few form.
  (. obj key arg) Call method named key on obj with arg as argument.
  (. obj -key) Get property named key on obj.
  (. obj -key val) Set property named key on obj to val."
  (function-with-name dot (obj key val)
    (var name (.substr (.valueOf key) 1))
    (return (if (.startsWith key "-")
      (if (=== val undefined)
        (quasiquote
          ((JSCode "(" (JSise (unquote obj)) ")." (unquote name))))
        (quasiquote
          ((=
            (JSCode "(" (JSise (unquote obj)) ")." (unquote name))
            (unquote val)))))
      (list (concat (list (new String (+ "." key)) obj) (.slice (argument-array) 2)))))))


(define-macro-with-comment define-array-accessor
  "~0 name.
   ~1 index."
  (syntax-template
    (define-macro ~0
      (syntax-template (array-get ~~0 ~1)))))

(define-array-accessor 1st 0)
(define-array-accessor 2nd 1)
(define-array-accessor 3rd 2)
(define-array-accessor 4th 3)
(define-array-accessor 5th 4)
(define-array-accessor 6th 5)
(define-array-accessor 7th 6)
(define-array-accessor 8th 7)
(define-array-accessor 9th 8)

(define-macro-with-comment repeat-times
  "~0 index
   ~1 max (exclusive)"
  (syntax-template
    (for (var ~0 0) (< ~0 ~1) (set! ~0 (+ ~0 1)) ~2-)))


(define-macro-with-comment for
  "~0  initializer
   ~1  condition
   ~2  increment
   ~3- contents"
  (syntax-template (JSCode "for(" ~0 ";" ~1 ";" ~2 "){"
    (JSise (statement-list (~3-)))
  "}")))
