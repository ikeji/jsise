var JSCode = new String('JSCode');
var JSise = new String('JSise');
JSCode.equals = JSise.equals = function (other) {
  return other instanceof String && other.valueOf() == this.valueOf();
};
function maybeDebugLog(str) {
  if (require) {
    return require('util').debuglog(str);
  } else {
    return function(mes) {};
  }
}

function parser(str) {
  var log = maybeDebugLog('jsise.parser');
  log('parser');
  log(str);
  var i = 0;         // Pos
  var state = null;  // Current state of parser.
  var parens = [[]];
  var token = "";

  var states = {
    paren: function paren() {
      var c = str[i];
      if (c == '(') {
        var paren = [];
        parens[0].push(paren);
        parens.unshift(paren);
      } else if (c == ' ' || c == '\n' || c == '\t') {
        return;
      } else if(c == ')'){
        if (parens.length == 1){
          throw new Error('too many closed paren');
        }
        parens.shift();
      } else if(c == '"') {
        state = states.string_literal;
      } else {
        state = states.id;
        state();
      }
    },
    id: function id() {
      var c = str[i];
      if (c == '(' || c == ')' || c == '\"' ||
          c == ' ' || c == '\n' || c == '\t') {
        parens[0].push(new String(token));
        token = '';
        state = states.paren;
        state();
      } else {
        token += c;
      }
    },
    string_literal: function string_literal() {
      var c = str[i];
      if (c == '\\') {
        state = states.esc;
      } else if (c == '\"') {
        parens[0].push(token);
        token = "";
        state = states.paren;
      } else {
        token += c;
      }
    },
    esc: function esc() {
      var c = str[i];
      if (c == 'n') {
        token  += '\n';
      } else if (c == 't') {
        token  += '\t';
      } else {
        token += c;
      }
      state = states.string_literal;
    }
  };
  state = states.paren;
  for(;i<str.length;i++) {
    state();
  }
  if (parens.length != 1){
    throw new Error('unclosed paren');
  }
  if (state == states.string_literal) {
    throw new Error('unclosed string');
  }
  return parens[0];
}

function macro(ast, opt_macros) {
  var log = maybeDebugLog('jsise.macro');
  if (!(ast instanceof Array)) return ast;
  function Macros(){};
  Macros.prototype = opt_macros || {};
  var macros = new Macros();
  // TODO: Define in root only.
  macros['defmacro'] = function(name, opt_bodys) {
    log('========== start defmacro : ' + name);
    log('========== with macro : ' + Object.keys(macros));
    log('========== body');
    var body = Array.prototype.slice.call(arguments, 1);
    log(body);
    log('========== /body');
    var ast = macro(body, macros);
    log('========== macro');
    log(ast);
    log('========== /macro');
    var ast = convert(ast);
    log('========== convert');
    log(ast);
    log('========== /convert');
    var code = codegen(ast);
    log('========== code');
    log(code);
    log('========== /code');
    var func = macros[name.valueOf()] = eval(code);
    log('========== func');
    log(func);
    log('========== /func');
    log('========== end defmacro : ' + name);
    return [];
  };
  return ast.reduce(function macroExpand(ret, exp) {
    if (!(exp instanceof Array) ||
        !(exp[0] instanceof String) ||
        !(macros[exp[0].valueOf()])) {
      return ret.concat([macro(exp, macros)]);
    } else {
      var name = exp[0].valueOf();
      log('========== start macro : ' + name);
      log('========== input');
      log(exp);
      log('========== /input');
      var out = macros[name].apply(undefined, exp.slice(1));
      log('========== output');
      log(out);
      log('========== /output');
      log('========== end macro : ' + name);
      return out.reduce(macroExpand, ret);
    }
  }, []);
}

function convert(ast) {
  var log = maybeDebugLog('jsise.convert');
  function convertExpression(exp) {
    log('=-=-=-=-=- convert');
    log(exp);
    if (typeof exp == 'string') {
      var esc = [JSCode, '"'];
      for(var i=0;i<exp.length;i++) {
        var c = exp.charAt(i);
        if (c=='"' || c == '\\') {
          esc.push('\\' + c);
        } else if (c == '\n') {
          esc.push('\\n');
        } else if (c == '\t') {
          esc.push('\\t');
        } else {
          esc.push(c);
        }
      }
      esc.push('"');
      return esc;
    }
    if (exp instanceof String) {
      return [JSCode, exp.valueOf()];
    }
    if (!(exp instanceof Array)) {
      log(exp);
      throw new Error('invalid ast:' + exp);
    }
    if (JSise.equals(exp[0])) {
      return [JSCode].concat(exp.slice(1).map(convertExpression));
    }
    if (JSCode.equals(exp[0])) {
      return exp.map(function(e, i) {
        if (i == 0 || typeof e == 'string') return e;
        else return convertExpression(e);
      });
    }
    var method, args;
    if (exp[0] instanceof String &&
        exp[0].valueOf()[0] == '.') {
      method = [JSCode, convertExpression(exp[1]), exp[0].valueOf()];
      args = exp.slice(2);
    } else {
      method = [JSCode, convertExpression(exp[0])];
      args = exp.slice(1);
    }
    return method.concat(['('],
        args.map(convertExpression).reduce(function(arr, v, i) {
          if (i != 0) {
            return arr.concat([',', v]);
          } else {
            return arr.concat([v]);
          }
        }, []), [')']);
  };
  return ast.map(function convertStatement(statement) {
    return [JSCode, convertExpression(statement), ';'];
  });
}

function codegen(ast) {
  var log = maybeDebugLog('jsise.codegen');
  return ast.map(function convertToJSCode(statement) {
    if (!statement instanceof Array ||
        !JSCode.equals(statement[0])) {
      log(statement);
      throw new Error('invalid code: ' + JSON.stringify(statement));
    }
    return statement.slice(1).map(function(code_or_statement) {
      if (typeof code_or_statement == 'string') {
        return code_or_statement;
      } else {
        return convertToJSCode(code_or_statement);
      }
    }).join("");
  }).join("\n\n");
}

function compile(src) {
  var log = maybeDebugLog('jsise');
  var ast = parser(src);
  log('----------parse');
  log(ast);
  log('----------/parse');
  var ast = macro(ast);
  log('----------macro');
  log(ast);
  log('----------/macro');
  var ast = convert(ast);
  log('----------convert');
  log(ast);
  log('----------/convert');
  var jscode = codegen(ast);
  log('----------codegen');
  log(jscode);
  log('----------/codegen');
  return jscode;
}
compile.parser = parser;
compile.macro = macro;
compile.convert = convert;
module.exports = compile;
